package br.com.desafioSupero.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.desafioSupero.api.model.Tarefa;
import br.com.desafioSupero.api.repository.tarefa.TarefaRepositoryQuery;

public interface TarefaRepository extends JpaRepository<Tarefa, Long>, TarefaRepositoryQuery {

}
