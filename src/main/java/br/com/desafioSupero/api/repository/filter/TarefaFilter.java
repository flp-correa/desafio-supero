package br.com.desafioSupero.api.repository.filter;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

public class TarefaFilter {
	
	private String titulo;
	private String descricao;
	private Boolean status;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataCriacaoInicio;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataCriacaoFim;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public LocalDate getDataCriacaoInicio() {
		return dataCriacaoInicio;
	}
	public void setDataCriacaoInicio(LocalDate dataCriacaoInicio) {
		this.dataCriacaoInicio = dataCriacaoInicio;
	}
	public LocalDate getDataCriacaoFim() {
		return dataCriacaoFim;
	}
	public void setDataCriacaoFim(LocalDate dataCriacaoFim) {
		this.dataCriacaoFim = dataCriacaoFim;
	}

}
