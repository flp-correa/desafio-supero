package br.com.desafioSupero.api.repository.tarefa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.desafioSupero.api.model.Tarefa;
import br.com.desafioSupero.api.model.Tarefa_;
import br.com.desafioSupero.api.repository.filter.TarefaFilter;

public class TarefaRepositoryImpl implements TarefaRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Tarefa> filtrar(TarefaFilter tarefaFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Tarefa> criteria = builder.createQuery(Tarefa.class);
		Root<Tarefa> root = criteria.from(Tarefa.class);
		
		Predicate[] predicates = criarRestricoes(tarefaFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<Tarefa> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(tarefaFilter));
	}
	
	private Predicate[] criarRestricoes(TarefaFilter tarefaFilter, CriteriaBuilder builder,
			Root<Tarefa> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(tarefaFilter.getTitulo())) {
			predicates.add(builder.like(
					builder.lower(root.get(Tarefa_.titulo)), "%" + tarefaFilter.getTitulo().toLowerCase() + "%"));
		}
		
		if (!StringUtils.isEmpty(tarefaFilter.getDescricao())) {
			predicates.add(builder.like(
					builder.lower(root.get(Tarefa_.descricao)), "%" + tarefaFilter.getDescricao().toLowerCase() + "%"));
		}
		
		if (tarefaFilter.getStatus() != null) {
			predicates.add(builder.equal(root.get(Tarefa_.status), tarefaFilter.getStatus()));
		}
		
		if (tarefaFilter.getDataCriacaoInicio() != null) {
			predicates.add(
					builder.greaterThanOrEqualTo(root.get(Tarefa_.dataCriacao), tarefaFilter.getDataCriacaoInicio()));
		}
		
		if (tarefaFilter.getDataCriacaoFim() != null) {
			predicates.add(
					builder.lessThanOrEqualTo(root.get(Tarefa_.dataCriacao), tarefaFilter.getDataCriacaoFim()));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<Tarefa> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(TarefaFilter tarefaFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Tarefa> root = criteria.from(Tarefa.class);
		
		Predicate[] predicates = criarRestricoes(tarefaFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
