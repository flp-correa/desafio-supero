package br.com.desafioSupero.api.repository.tarefa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.desafioSupero.api.model.Tarefa;
import br.com.desafioSupero.api.repository.filter.TarefaFilter;

public interface TarefaRepositoryQuery {

	public Page<Tarefa> filtrar(TarefaFilter tarefaFilter, Pageable pageable);
}
