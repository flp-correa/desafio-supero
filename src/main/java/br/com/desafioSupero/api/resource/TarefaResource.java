package br.com.desafioSupero.api.resource;

import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.desafioSupero.api.event.RecursoCriadoEvent;
import br.com.desafioSupero.api.model.Tarefa;
import br.com.desafioSupero.api.repository.TarefaRepository;
import br.com.desafioSupero.api.repository.filter.TarefaFilter;
import br.com.desafioSupero.api.service.TarefaService;

@RestController
@RequestMapping("/tarefas")
public class TarefaResource {
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private TarefaService tarefaService;
	
	@Autowired
	private TarefaRepository tarefaRepository; 
	
	@GetMapping
	public Page<Tarefa> pesquisar(TarefaFilter tarefaFilter, Pageable pageable) {
		return tarefaRepository.filtrar(tarefaFilter, pageable);
	}
	
	@GetMapping("/{codigo}")
	public ResponseEntity<Tarefa> buscarPeloCodigo(@PathVariable Long codigo) {
		Optional<Tarefa> optionalTarefa = tarefaRepository.findById(codigo);
		return optionalTarefa.isPresent() ? ResponseEntity.ok(optionalTarefa.get()) : ResponseEntity.notFound().build();
	}

	@PostMapping("/nova")
	public ResponseEntity<Tarefa> criar(@Valid @RequestBody Tarefa tarefa, HttpServletResponse response) {
		Tarefa tarefaSalvo = tarefaService.salvar(tarefa);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, tarefaSalvo.getCodigo()));
		return ResponseEntity.status(HttpStatus.CREATED).body(tarefaSalvo);
	}
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long codigo) {
		tarefaRepository.deleteById(codigo);
	}

	@PutMapping("/{codigo}")
	public ResponseEntity<Tarefa> atualizar(@PathVariable Long codigo, @Valid @RequestBody Tarefa tarefa) {
		Tarefa tarefaSalva = tarefaService.atualizar(codigo, tarefa);
		return ResponseEntity.ok(tarefaSalva);
	}
	
	@PutMapping("/{codigo}/status")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void atualizarStatus(@PathVariable Long codigo, @RequestBody Boolean status) {
		tarefaService.atualizarStatus(codigo, status);
	}
}
