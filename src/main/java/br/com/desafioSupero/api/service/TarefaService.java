package br.com.desafioSupero.api.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.desafioSupero.api.model.Tarefa;
import br.com.desafioSupero.api.repository.TarefaRepository;

@Service
public class TarefaService {

	@Autowired
	private TarefaRepository tarefaRepository;
	
	public Tarefa salvar(Tarefa tarefa) {
		return tarefaRepository.save(tarefa);
	}

	public Tarefa atualizar(Long codigo, Tarefa tarefa) {
		Tarefa tarefaSalva = buscarTarefaPeloCodigo(codigo);
		BeanUtils.copyProperties(tarefa, tarefaSalva, "codigo");
		return tarefaRepository.save(tarefaSalva);
	}
	
	public void atualizarStatus(Long codigo, Boolean ativo) {
		Tarefa tarefaSalva = buscarTarefaPeloCodigo(codigo);
		tarefaSalva.setStatus(ativo);
		tarefaRepository.save(tarefaSalva);
	}
	
	private Tarefa buscarTarefaPeloCodigo(Long codigo) {
		Optional<Tarefa> tarefaSalva = tarefaRepository.findById(codigo);
		
		if (!tarefaSalva.isPresent()) {
			throw new EmptyResultDataAccessException(1);
		}
		return tarefaSalva.get();
	}
}
