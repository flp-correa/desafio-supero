package br.com.desafioSupero.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import br.com.desafioSupero.api.config.property.DesafioSuperoApiProperty;

@SpringBootApplication
@EnableConfigurationProperties(DesafioSuperoApiProperty.class)
public class DesafioSuperoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioSuperoApplication.class, args);
	}
}
