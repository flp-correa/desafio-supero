package br.com.desafioSupero.api.config.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("desafio-supero")
public class DesafioSuperoApiProperty {
	
	private String originPermitida = "http://localhost:4200";
	
	public String getOriginPermitida() {
		return originPermitida;
	}

	public void setOriginPermitida(String originPermitida) {
		this.originPermitida = originPermitida;
	}

}
